function validatorForm(value, error) {
    if(value.length === 0) {
        document.getElementById(error).innerHTML = 'Vui lòng nhập trường này';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
};

function validatorUsername(value, error) {
    var regexUsername = /^[a-z0-9_-]{4,6}$/;
    if(!regexUsername.test(value)) {
        document.getElementById(error).innerHTML = 'Tài khoản phải đủ 4-6 ký tự';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
};

function validatorSameAccountName(value, list, error) {
    var indexListUsername = list.findIndex(function(index) {
        return index.username == value;
    });
    if(indexListUsername == -1) {
        document.getElementById(error).innerHTML = '';
        return true;
    } else {
        document.getElementById(error).innerHTML = 'Tài khoản phải đã tồn tại';
        return false;
    };
};


function validatorName(value, error) {
    var regexName = /^[^\d+]*[\d+]{0}[^\d+]*$/;
    if(!regexName.test(value)) {
        document.getElementById(error).innerHTML = 'Tên nhân viên phải là chữ';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
};

function validatorEmail(value, error) {
    var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(!regexEmail.test(value)) {
        document.getElementById(error).innerHTML = 'Email không đúng định dạng';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
};

function validatorPassWord(value, error) {
    var regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    if(!regexPassword.test(value)) {
        document.getElementById(error).innerHTML = 'Mật khẩu phải chữ 6-10 ký tự, ít nhất 1 số, 1 ký tự in hoa, 1 ký tự đặc biệt';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
};

function validatorIncome(value, error, min, max) {
    if(value >= min && value <= max) {
        document.getElementById(error).innerHTML = '';
        return true;
    } else {
        document.getElementById(error).innerHTML = 'Lương cơ bản quá thấp hoặc quá cao';
        return false;
    };
};

function validatorTimeWorking(value, error) {
    if(value >= 80 && value <= 200) {
        document.getElementById(error).innerHTML = '';
        return true;
    } else {
        document.getElementById(error).innerHTML = 'Giờ làm trong tháng quá ít hoặc quá nhiều';
        return false;
    };
};

function validatorPosition(value, error) {
    if(value == '') {
        document.getElementById(error).innerHTML = 'Vui lòng chọn chức vụ';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
};

function validatorDate(value, error) {
    var regexDate = /^[0-3]?[0-9]\/[01]?[0-9]\/[12][90][0-9][0-9]$/;
    if(!regexDate.test(value)) {
        document.getElementById(error).innerHTML = 'Ngày tháng năm phải đúng định dạng mm/dd/yyyy';
        return false;
    } else {
        document.getElementById(error).innerHTML = '';
        return true;
    };
}