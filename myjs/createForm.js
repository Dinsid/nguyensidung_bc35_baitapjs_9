function createFormInput() {
    var username = document.getElementById('username').value.trim();
    var name = document.getElementById('name').value.trim();
    var email = document.getElementById('email').value.trim();
    var password = document.getElementById('password').value.trim();
    var datepicker = document.getElementById('datepicker').value;
    var basicIncome = document.getElementById('basic-income').value.trim();
    var position = document.getElementById('position').value;
    var workingTime = document.getElementById('gioLam').value.trim();

    var user = new User(username, name, email, password, datepicker, basicIncome, position, workingTime);
    return user;
}

function renderListUsers(list) {
    var contentHTML = '';
    
    for(var i = 0; i < list.length; i++) {
        var listUser = list[i];
        var classTr = `
        <tr>
            <td>${listUser.username}</td>
            <td>${listUser.name}</td>
            <td>${listUser.email}</td>
            <td>${listUser.datepicker}</td>
            <td>${listUser.rank()}</td>
            <td>${listUser.totalIncome()}</td>
            <td>${listUser.employeeRating()}</td>
            <td>
                <button class="btn btn-primary" id="update" onclick="getUpdateUser('${listUser.username}')" data-toggle="modal" data-target="#myModal">Sửa</button>
                <button class="btn btn-danger" onclick="deleteUser('${listUser.username}')">Xóa</button>
            </td>
        </tr>
        `;
        contentHTML += classTr;
    };
    document.getElementById('tableDanhSach').innerHTML = contentHTML;
}

function closeModalResetForm() {
    var addUser = document.getElementById('btnThemNV');
    addUser.setAttribute('data-target', '#myModal');
    addUser.setAttribute('data-toggle', 'modal');
    document.getElementById('form-form').reset();
}

function closeModalResetFormUpdate() {
    var addUser = document.getElementById('btnCapNhat');
    addUser.setAttribute('data-target', '#myModal');
    addUser.setAttribute('data-toggle', 'modal');
    document.getElementById('form-form').reset();
}

function noneBtn() {
    var addBtn = document.getElementById('btnThemNV');
    addBtn.style.display = 'none';
    var BtnUpdate = document.getElementById('btnCapNhat');
    BtnUpdate.style.display = 'block';
}

function showInfoUser(user) {
    document.getElementById('username').value = user.username;
    document.getElementById('name').value = user.name;
    document.getElementById('email').value = user.email;
    document.getElementById('password').value = user.password;
    document.getElementById('datepicker').value = user.datepicker;
    document.getElementById('basic-income').value = user.basicIncome;
    document.getElementById('position').value = user.position;
    document.getElementById('gioLam').value = user.workingTime;
}

function openModalAddUser() {
    var updateBtn = document.getElementById('btnCapNhat');
    updateBtn.style.display = 'none';
}

function closeModal() {
    document.getElementById('form-form').reset();
}