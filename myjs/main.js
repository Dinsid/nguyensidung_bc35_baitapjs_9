var USER = 'USER';
var listUsers = [];

var checkUsers = localStorage.getItem(USER);
if(checkUsers != null) {
    var dataUSers = JSON.parse(checkUsers);

    listUsers = dataUSers.map(function(user) {
        return new User(
            user.username,
            user.name,
            user.email,
            user.password,
            user.datepicker,
            user.basicIncome,
            user.position,
            user.workingTime
        );
    });
    renderListUsers(listUsers);
};

function savaUsersInLocalStorage() {
    var userJSON = JSON.stringify(listUsers);
    localStorage.setItem(USER, userJSON);
};

function addUser() {
    var newUser = createFormInput();

    var isValidate = true;
    isValidate &= validatorForm(newUser.username, 'tbTKNV') && validatorUsername(newUser.username, 'tbTKNV') && validatorSameAccountName(newUser.username, listUsers, 'tbTKNV');
    isValidate &= validatorForm(newUser.name, 'tbTen') && validatorName(newUser.name, 'tbTen');
    isValidate &= validatorForm(newUser.email, 'tbEmail') && validatorEmail(newUser.email, 'tbEmail');
    isValidate &= validatorForm(newUser.password, 'tbMatKhau') && validatorPassWord(newUser.password, 'tbMatKhau');
    isValidate &= validatorForm(newUser.workingTime, 'tbGiolam') && validatorTimeWorking(newUser.workingTime, 'tbGiolam');
    isValidate &= validatorForm(newUser.basicIncome, 'tbLuongCB') && validatorIncome(newUser.basicIncome, 'tbLuongCB', 1000000, 20000000);
    isValidate &= validatorPosition(newUser.position, 'tbChucVu');
    isValidate &= validatorDate(newUser.datepicker, 'tbNgay');
    
    if(isValidate) {
        listUsers.push(newUser);
        renderListUsers(listUsers);
        savaUsersInLocalStorage();
        closeModalResetForm();
    };
};

function getUpdateUser(username) {
    noneBtn();
    var indexUser = listUsers.findIndex(function(user) {
        return user.username == username;
    });

    if(indexUser == -1) return;
    var user = listUsers[indexUser];
    showInfoUser(user);
    document.getElementById('username').disabled = true;
};

function updateUser() {
    var getUserUpdate = createFormInput();

    var indexUser = listUsers.findIndex(function(user) {
        return user.username == getUserUpdate.username;
    });

    if(indexUser == -1) return;
    var isValidate = true;

    isValidate &= validatorForm(getUserUpdate.name, 'tbTen') && validatorName(getUserUpdate.name, 'tbTen');
    isValidate &= validatorForm(getUserUpdate.email, 'tbEmail') && validatorEmail(getUserUpdate.email, 'tbEmail');
    isValidate &= validatorForm(getUserUpdate.password, 'tbMatKhau') && validatorPassWord(getUserUpdate.password, 'tbMatKhau');
    isValidate &= validatorForm(getUserUpdate.workingTime, 'tbGiolam') && validatorTimeWorking(getUserUpdate.workingTime, 'tbGiolam');
    isValidate &= validatorForm(getUserUpdate.basicIncome, 'tbLuongCB') && validatorIncome(getUserUpdate.basicIncome, 'tbLuongCB', 1000000, 20000000);
    isValidate &= validatorPosition(getUserUpdate.position, 'tbChucVu');
    isValidate &= validatorDate(getUserUpdate.datepicker, 'tbNgay');

    if(isValidate) {
        listUsers[indexUser] = getUserUpdate;
        renderListUsers(listUsers);
        savaUsersInLocalStorage();
        closeModalResetFormUpdate();
        document.getElementById('username').disabled = false;
    };
};

function deleteUser(username) {
    var indexUser = listUsers.findIndex(function(user) {
        return user.username == username;
    });

    if(indexUser == -1) return;
    listUsers.splice(indexUser, 1);
    renderListUsers(listUsers);
    savaUsersInLocalStorage();
};

function searchUser() {
    var valueSearchInput = document.getElementById('searchName').value;
    var valueSearch = listUsers.filter(function(value) {
        return value.employeeRating().toUpperCase().includes(valueSearchInput.toUpperCase());
    });
    renderListUsers(valueSearch);
};