function User(_username, _name, _email, _password, _datepicker, _basicIncome, _position, _workingTime) {
    this.username = _username;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.datepicker = _datepicker;
    this.basicIncome = _basicIncome;
    this.position = _position;
    this.workingTime = _workingTime;
    this.employeeRating = function() {
        if(this.workingTime >= 192) {
            return 'Xuất sắc';
        } else if(this.workingTime >= 176 && this.workingTime < 192) {
            return 'Giỏi';
        } else if(this.workingTime >= 160 && this.workingTime < 176) {
            return 'Khá';
        } else {
            return 'Trung bình';
        };
    };
    this.totalIncome = function() {
        switch(this.position) {
            case '1':
                var totalIncome = this.basicIncome * 3;
               return new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'VND' }).format(totalIncome);;
            case '2':
                var totalIncome = this.basicIncome * 2;
                return new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'VND' }).format(totalIncome);;
            case '3':
                var totalIncome = this.basicIncome * 1;
                return new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'VND' }).format(totalIncome);;
        };
    };
    this.rank = function() {
        switch(this.position) {
            case '1':
                return 'Giám đốc';
            case '2':
                return 'Trưởng phòng';
            case '3':
                return 'Nhân viên'; 
        };
    };
};